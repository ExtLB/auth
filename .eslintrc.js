module.exports = {
  "env": {
    "es6": true,
    "node": true
  },
  "extends": "loopback",
  "parserOptions": {
    "ecmaVersion": 2018,
    "sourceType": "module"
  },
  "rules": {
    "indent": [
      "error",
      2
    ],
    "quotes": [
      "error",
      "single"
    ],
    "semi": [
      "error",
      "always"
    ],
    "no-console": "off",
    "no-undef": "off"
  },
  "parser": "typescript-eslint-parser",
  "plugins": ["typescript", "babel", "jasmine"]
};
