Ext.define('Client.controller.Authentication', {
  extend: 'Ext.app.Controller',

  loggedIn: false,
  user: {},
  localLogin: (email, password) => {
    let Loader = Client.app.getController('Loader');
    let Authentication = Client.app.getController('Authentication');
    let request = {};
    if (Loader.serverInfo.passport === true) {
      request = {
        url: '/auth/local',
        params: {email: email, password: password},
        method: 'POST'
      };
    } else {
      request = {
        url: '/api/v1/users/login',
        params: {email: email, password: password},
        method: 'POST'
      };
    }
    return Ext.Ajax.request(request).then(request => {
      let res = Ext.decode(request.responseText, true);
      if (Loader.serverInfo.passport === true) {
        if (request.status === 200 && res.success === true) {
          return Authentication.getUser().then(success => {
            if (success === true) {
              Authentication.fireEvent('login');
            }
            return success;
          });
        } else {
          return 'Authentication Failed!';
        }
      } else {
        if (request.status === 200) {
          return Authentication.getUser().then(success => {
            if (success === true) {
              Authentication.fireEvent('login');
            }
            return success;
          });
        } else {
          return 'Authentication Failed!';
        }
      }
    });
  },
  register: (username, email, password) => {
    let Authentication = Client.app.getController('Authentication');
  },
  loginRemote: (url) => {
    window.location.href = url;
  },
  logout: () => {
    let Loader = Client.app.getController('Loader');
    let Authentication = Client.app.getController('Authentication');
    try {
      Client.app.originalHash = window.location.hash.substring(1);
    } catch (err) {
      console.error(err);
    }
    Authentication.loggedIn = false;
    return Ext.Ajax.request({
      url: '/api/v1/Users/logout',
      method: 'POST'
    }).then(() => {
      Authentication.fireEvent('logout');
      if (Loader.serverInfo.auth.loginUrl.startsWith('#')) {
        Authentication.redirectTo(Loader.serverInfo.auth.loginUrl.substr(1));
        return true;
      } else {
        window.location.href = Loader.serverInfo.auth.loginUrl;
        return false;
      }
    });
  },
  getUser: () => {
    let Authentication = Client.app.getController('Authentication');
    if (Authentication.loggedIn === true && !_.isUndefined(Authentication.user.id)) {
      return new Ext.Promise((resolve) => {
        resolve(true);
      });
    }
    return Ext.Ajax.request({
      url: '/api/v1/users/me',
      method: 'GET',
      params: {
        filter: '{"include": ["roles"]}'
      },
    }).then(response => {
      let res = Ext.decode(response.responseText, true);
      if (res.id) {
        Authentication.loggedIn = true;
        Authentication.user = res;
        return true;
      } else {
        Authentication.loggedIn = false;
        Authentication.user = {};
        return false;
      }
    });
  },
  check: () => {
    let Authentication = Client.app.getController('Authentication');
    return new Ext.Promise((resolve) => {
      if (Authentication.loggedIn === true) {
        resolve(true);
      } else {
        let accessToken = Ext.util.Cookies.get('accessToken');
        if (_.isNull(accessToken)) {
          resolve(false);
        } else {
          Authentication.getUser().then(success => {
            if (success) {
              Authentication.fireEvent('login');
            }
            resolve(success);
          }).catch(err => {
            console.error(err);
            resolve(false);
          })
        }
      }
    });
  },
  init: (app) => {
    let Authentication = app.getController('Authentication');
    let Loader = app.getController('Loader');
    Loader.on('completed', () => {
      if (Loader.serverInfo.auth.enabled === true) {
        if (Loader.serverInfo.loggedIn === true) {
          return true;
        } else {
          if (Loader.serverInfo.auth.loginUrl.startsWith('#')) {
            app.hash = Loader.serverInfo.auth.loginUrl.substr(1);
            return true;
          } else {
            window.location.href = Loader.serverInfo.auth.loginUrl;
            return false;
          }
        }
      } else {
        return true;
      }
    });
    Loader.tasks.push({
      name: 'mod-auth:AUTHENTICATION',
      function: Authentication.check
    });
  }
});
