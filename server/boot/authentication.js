"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
var boot_script_1 = require("@mean-expert/boot-script");
var async = require("async");
var log = require('@dosarrest/loopback-component-logger')('server/boot/authentication');
;
var Authentication = (function () {
    function Authentication(options) {
        var me = this;
        var app = options.app;
        var callback = options.callback;
        me.app = app;
        var authOptions = app.get('auth');
        if (authOptions.enabled === true) {
            app.enableAuth();
            var ACLModel_1 = me.app.registry.getModelByType('ACL');
            var UserModel_1 = me.app.registry.getModelByType('User');
            var RoleModel_1 = me.app.registry.getModelByType('Role');
            var RoleMappingModel_1 = me.app.registry.getModelByType('RoleMapping');
            RoleMappingModel_1.nestRemoting('user');
            RoleModel_1.nestRemoting('users');
            app.on('started', function () {
                if (authOptions.setupDefaultUsers === true) {
                    log.info('--- Setting up default users');
                    var countChecks = {
                        roles: function (cb) {
                            RoleModel_1.count({}, cb);
                        },
                        roleMappings: function (cb) {
                            RoleMappingModel_1.count({}, cb);
                        },
                        users: function (cb) {
                            UserModel_1.count({}, cb);
                        }
                    };
                    async.series(countChecks, function (countErr, counts) {
                        if (counts.roles === 0 && counts.roleMappings === 0 && counts.users === 0) {
                            async.series({
                                administrator: function (cb) {
                                    RoleModel_1.findOrCreate({
                                        where: {
                                            name: "Administrator"
                                        }
                                    }, {
                                        name: "Administrator",
                                        description: "Administrators",
                                        created: new Date(),
                                        modified: new Date()
                                    }, cb);
                                },
                                developer: function (cb) {
                                    RoleModel_1.findOrCreate({
                                        where: {
                                            name: "Developer"
                                        }
                                    }, {
                                        name: "Developer",
                                        description: "Developers",
                                        created: new Date(),
                                        modified: new Date()
                                    }, cb);
                                }
                            }, function (err, roles) {
                                log.info('New Roles Created:', roles);
                                async.series({
                                    administrator: function (cb) {
                                        return UserModel_1.findOrCreate({
                                            where: {
                                                "username": "Administrator"
                                            }
                                        }, {
                                            "username": "Administrator",
                                            "email": "admin@email.com",
                                            "emailVerified": true,
                                            "password": "test"
                                        }, cb);
                                    },
                                    developer: function (cb) {
                                        return UserModel_1.findOrCreate({
                                            where: {
                                                "username": "Developer"
                                            }
                                        }, {
                                            "username": "Developer",
                                            "email": "developer@email.com",
                                            "emailVerified": true,
                                            "password": "test"
                                        }, cb);
                                    }
                                }, function (uErr, users) {
                                    log.info('New Users Created:', users);
                                    async.series({
                                        "administrator-Administrator": function (cb) {
                                            RoleMappingModel_1.findOrCreate({
                                                principalType: RoleMappingModel_1.USER,
                                                principalId: users.administrator[0].id,
                                                roleId: roles.administrator[0].id
                                            }, cb);
                                        },
                                        "developer-Developer": function (cb) {
                                            RoleMappingModel_1.findOrCreate({
                                                principalType: RoleMappingModel_1.USER,
                                                principalId: users.developer[0].id,
                                                roleId: roles.developer[0].id
                                            }, cb);
                                        }
                                    }, function (err, addedMappings) {
                                        log.info('Added Role Mappings:', addedMappings);
                                        log.info('--- Setting up default users completed!');
                                        app.emit('authenticationLoaded');
                                    });
                                });
                            });
                        }
                    });
                    ACLModel_1.findOrCreate({
                        model: UserModel_1.definition.name,
                        accessType: '*',
                        principalType: 'ROLE',
                        principalId: 'Administrator',
                        permission: 'ALLOW',
                        property: '*'
                    }).then(function (acl) {
                    })["catch"](function (err) {
                        log.error(err);
                    });
                }
                else {
                    app.emit('authenticationLoaded');
                }
            });
            callback(null, true);
        }
        else {
            callback(null, true);
        }
    }
    Authentication = __decorate([
        boot_script_1.BootScript()
    ], Authentication);
    return Authentication;
}());
module.exports = Authentication;
//# sourceMappingURL=authentication.js.map