import * as _ from 'lodash';
import {BootScript} from "@mean-expert/boot-script";
import {ACL, LoopBackApplication, User} from "loopback";
import * as async from 'async';
let log = require('@dosarrest/loopback-component-logger')('server/boot/authentication');

interface Options {
  app: LoopBackApplication,
  callback: Function
};

@BootScript()
class Authentication {
  app: LoopBackApplication;
  constructor(options: Options) {
    let me = this;
    let app = options.app;
    let callback = options.callback;
    me.app = app;
    let authOptions = app.get('auth');
    if (authOptions.enabled === true) {
      app.enableAuth();
      // let countMixin = require('../../../../../server/mixins/count');
      // let extQueryMixin = require('../../../../../server/mixins/ext-query');
      let ACLModel = (me.app as any).registry.getModelByType('ACL');
      let UserModel = (me.app as any).registry.getModelByType('User');
      let RoleModel = (me.app as any).registry.getModelByType('Role');
      let RoleMappingModel = (me.app as any).registry.getModelByType('RoleMapping');
      // UserModel.hasMany(RoleModel, {through: RoleMappingModel, foreignKey: 'principalId'});
      // countMixin(UserModel, {});
      // extQueryMixin(UserModel, {});
      // RoleMappingModel.belongsTo(UserModel, {foreignKey: 'principalId', as: 'user'});
      RoleMappingModel.nestRemoting('user');
      // RoleModel.hasMany(UserModel, {through: RoleMappingModel, foreignKey: 'roleId', as: 'users'});
      RoleModel.nestRemoting('users');
      app.on('started', () => {
        if (authOptions.setupDefaultUsers === true) {
          log.info('--- Setting up default users');
          let countChecks: async.Dictionary<async.AsyncFunction<{},{}>> = {
            roles: (cb) => {
              RoleModel.count({}, cb);
            },
            roleMappings: (cb) => {
              RoleMappingModel.count({}, cb);
            },
            users: (cb) => {
              UserModel.count({}, cb);
            }
          };
          async.series(countChecks, (countErr, counts) => {
            if (counts.roles === 0 && counts.roleMappings === 0 && counts.users === 0) {
              async.series({
                administrator: (cb) => {
                  RoleModel.findOrCreate({
                    where: {
                      name: "Administrator",
                    }
                  }, {
                    name: "Administrator",
                    description: "Administrators",
                    created: new Date(),
                    modified: new Date()
                  }, cb);
                },
                developer: (cb) => {
                  RoleModel.findOrCreate({
                    where: {
                      name: "Developer",
                    }
                  }, {
                    name: "Developer",
                    description: "Developers",
                    created: new Date(),
                    modified: new Date()
                  }, cb);
                }
              }, (err, roles) => {
                log.info('New Roles Created:', roles);
                async.series({
                  administrator: (cb) => {
                    return UserModel.findOrCreate({
                      where: {
                        "username": "Administrator",
                      }
                    }, {
                      "username": "Administrator",
                      "email": "admin@email.com",
                      "emailVerified": true,
                      "password": "test",
                    }, cb);
                  },
                  developer: (cb) => {
                    return UserModel.findOrCreate({
                      where: {
                        "username": "Developer",
                      }
                    }, {
                      "username": "Developer",
                      "email": "developer@email.com",
                      "emailVerified": true,
                      "password": "test",
                    }, cb);
                  }
                }, (uErr, users) => {
                  log.info('New Users Created:', users);
                  async.series({
                    "administrator-Administrator": (cb) => {
                      RoleMappingModel.findOrCreate({
                        principalType: RoleMappingModel.USER,
                        principalId: (users.administrator as any)[0].id,
                        roleId: (roles.administrator as any)[0].id
                      }, cb);
                    },
                    "developer-Developer": (cb) => {
                      RoleMappingModel.findOrCreate({
                        principalType: RoleMappingModel.USER,
                        principalId: (users.developer as any)[0].id,
                        roleId: (roles.developer as any)[0].id
                      }, cb);
                    }
                  }, (err, addedMappings) => {
                    log.info('Added Role Mappings:', addedMappings);
                    log.info('--- Setting up default users completed!');
                    app.emit('authenticationLoaded');
                  });
                });
              });
            }
          });
          ACLModel.findOrCreate({
            model: UserModel.definition.name,
            accessType: '*',
            principalType: 'ROLE',
            principalId: 'Administrator',
            permission: 'ALLOW',
            property: '*'
          }).then((acl: ACL) => {
          }).catch((err: any) => {
            log.error(err);
          });
        } else {
          app.emit('authenticationLoaded');
        }
      });
      callback(null, true);
    } else {
      callback(null, true);
    }
  }
}
module.exports = Authentication;

// 'use strict';
//
// module.exports = function enableAuthentication(server) {
//   // enable authentication
//   server.enableAuth();
//   server.registry.getModelByType('User').create({
//     "username": "nam",
//     "email": "some@email.com",
//     "emailVerified": true,
//     "password": "test"
//   });
// };
