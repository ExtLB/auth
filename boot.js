let Authentication = require('./server/boot/authentication');
const async = require('async');
let log = require('@dosarrest/loopback-component-logger')('mod-auth/boot');
class Boot {
  constructor(app, done) {
    let me = this;
    me.app = app;
    me.done = done;
    me.init();
  }
  setupAuthentication(cb) {
    let me = this;
    let app = me.app;
    new Authentication({app: app, callback: cb});
  }
  init() {
    let me = this;
    let done = me.done;
    async.series({
      setupAuthentication: me.setupAuthentication.bind(me)
    }, (err) => {
      if (err) console.log(err);
      done(null, true);
    });
    // done(null, true);
  }
}
module.exports = Boot;
