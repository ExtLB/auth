class Module {
  constructor() {
  }
  boot(app, callback) {
    const Boot = require('./boot');
    new Boot(app, callback);
  }
  install(configs) {
    const installer = require('./installer');
    return installer(configs);
  }
}
module.exports = Module;
